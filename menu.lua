local composer = require( "composer" )
local widget = require( "widget" )
local json = require "json"
local menuList = require "scripts.menulist"
local scene = composer.newScene()



local background,menuBg,headBg,head
local name,account,shadow

local menuTable = {}
local menuNTable = {}
local menuIconTable = {}
local menuBgTable = {}

local menuGroups = {}
local menuAllGroup =nil
local menuListItem ={
	{name=myapp.title[2],page="",noUse=false,sub={}},
	{name=myapp.title[2],page="",noUse=false,sub={}},
	{name=myapp.title[3],page="",noUse=false,sub={}},
	{name=myapp.title[4],page="",noUse=false,sub={}},

	}




--actionBar高度
local titleHeight

function scene:create( event )
	local group = self.view
	local pageName =event.params.data
	local fbData
	titleHeight = event.params.tHeight


	menuAllGroup =display.newGroup()
	--灰色背景
	background = display.newRect( 0, 0,display.actualContentWidth , display.actualContentHeight )  
	background.anchorX=1
	background:setFillColor(0,0.5)  
	background.x=display.screenOriginX + display.actualContentWidth
	background.y = display.screenOriginY + background.contentHeight * 0.5+display.topStatusBarContentHeight
	background.alpha = 0
	group:insert(background)





	--側邊欄背景
	menuBg = display.newRect( 0, 0,display.actualContentWidth/1.1 , display.actualContentHeight )  
 	menuBg:setFillColor(1)
	menuBg.x=-menuBg.width/2
	menuBg.y = display.screenOriginY + menuBg.contentHeight * 0.5+display.topStatusBarContentHeight
	menuBg:addEventListener( "tap", function ( event )

		return true
	end )

	group:insert(menuBg)


	--帳號背景
	headBg = display.newImageRect("images/account_background.jpg",menuBg.width,menuBg.width*0.51 )
	headBg.x = menuBg.x
	headBg.y = display.screenOriginY + headBg.contentHeight * 0.5+display.topStatusBarContentHeight
	group:insert(headBg)

	--大頭
	local paint = {
	    type = "image",
	    filename = "Icon-xxxhdpi.png",
	    baseDir=system.ResourceDirectory,
	}


	head = display.newCircle( 0,0,28)
	head.x=halfW
	head.fill = paint
	head.anchorY=0
	head.x = menuBg.x-menuBg.width/4
	head.y = menuBg.y-menuBg.height/2+10
	group:insert(head)

	-- http://graph.facebook.com/myapp.fbid/picture?redirect=0&height=200&type=normal&width=200


	--取得各表影片數量
	local function getListCount()
		local function networkListener( event )
		  if ( event.isError ) then
		  else
		  	local d = json.decode(event.response)

		  		myapp.videoCount =d.videosum
		  		myapp.mealCount = d.mealsum
		  		myapp.favoCount =d.famealsum

		  		--店家數量
		  		if myapp.mealCount~=0 then
		  			menuNTable[1].text =tostring(myapp.mealCount)
		  		end

		  		--最愛數量
		  		if myapp.favoCount~=0 then
		  			menuNTable[2].text =tostring(myapp.favoCount)
		  		end

		  		--影片數量
		  		if myapp.videoCount~=0 then
		  			menuNTable[3].text =tostring(myapp.videoCount)
		  		end


		  			-- --學習狀態
		  			-- menuNTable[3].text =tostring(myapp.count.watchCount)

		  			-- --最愛
		  			-- menuNTable[4].text =tostring(myapp.count.favoCount)
			  		-- else
			  		-- 	myapp.count.newCount =luacode.coin(d.sum)

			  		-- 	--全部課堂
			  		-- 	if myapp.count.newCount~="0" then
			  		-- 		menuNTable[2].text =tostring(myapp.count.newCount).."+"
			  		-- 	else
			  		-- 		menuNTable[2].text =tostring(myapp.count.newCount)
			  		-- 	end

		  end
		end

			network.request(myapp.COUNT_URL,"GET",networkListener,myapp.reParams )
	end

	--取得大頭照
	local function getPicture( )
		
		if myapp.fbid~="" and myapp.headRead==false then

			  local function loadImageComplete( event )
			  if ( event.isError ) then
			    native.showAlert( myapp.strings.msg, myapp.strings.netError, { myapp.strings.check },errorCheck )
			  elseif ( event.phase == "began" ) then
			  elseif ( event.phase == "ended" ) then

			  	paint.filename =event.response.filename
			  	paint.baseDir=event.response.baseDirectory
			  	head.fill = paint
			  	transition.to( head, { alpha = 1.0 } )
			  	myapp.headRead=true
			  end
			end


			function loadImage(imgPath,picName)
			  local path = system.pathForFile(picName, system.TemporaryDirectory )
			  local fhd = io.open( path )
			  if fhd then

			  	--有圖片了
			  	fhd:close()

			  	paint.filename =picName
			  	paint.baseDir=system.TemporaryDirectory
			    head.fill = paint
			    transition.to( head, { alpha = 1.0 } )
			    myapp.headRead=true

			   else
			    --沒圖片，開啟下載
			    if(imgPath~="") then

			      
			      network.download(
			        imgPath,
			        "GET",
			        loadImageComplete,
			        myapp.downParams,
			        picName,
			        system.TemporaryDirectory
			        )
			    end
			  end
			end


			local function networkListener( event )
			  if ( event.isError ) then
			        native.showAlert( myapp.strings.msg, myapp.strings.netError, { myapp.strings.check } )
			  elseif ( event.phase == "ended" ) then

			  		local temp = json.decode(event.response)
			  		if temp.data.url~=nil then

			  		 	loadImage(temp.data.url,"p"..myapp.fbid)
			  		end

			  end
			end

			network.request("http://graph.facebook.com/"..myapp.fbid.."/picture?redirect=0&height=200&type=normal&width=200","GET",networkListener,myapp.reParams )
			
		elseif myapp.fbid~=nil and myapp.headRead then

			local path = system.pathForFile("p"..myapp.fbid, system.TemporaryDirectory )
			local fhd = io.open( path )
			if fhd then
				--有圖片了
				fhd:close()
				paint.filename ="p"..myapp.fbid
				paint.baseDir=system.TemporaryDirectory
				head.fill = paint
				transition.to( head, { alpha = 1.0 } )
			end
		end
	end



	-- getPicture()

	--帳號反倒影
	shadow = display.newRect( -headBg.width/2, headBg.y+headBg.height/2, headBg.width, headBg.height*0.4 )
	shadow.anchorY=1
	shadow.fill = myapp.menuShadowPaint
	group:insert(shadow)


	--姓名
	name = display.newText(group,"", 0, 0, native.systemFont, 14 )
	name:setFillColor( 255 )
	name.anchorX =0
	name.anchorY =1
	name.x =menuBg.x-menuBg.width/2
	
	--帳號
	account = display.newText(group,"", 0, 0, native.systemFont, 12 )
	account:setFillColor( 255 )
	account.anchorX =0
	account.anchorY=1
	account.x =menuBg.x-menuBg.width/2
	account.y = shadow.y-5

	name.y=account.y-account.height

	
	-- if myapp.userToken ~=nil and myapp.userToken~="" then

	-- 	name.text =myapp.userName
	-- 	account.text =myapp.userAccount
	-- else
	-- 	name.text =myapp.strings.noLogin
	-- end
	getListCount()



	list = widget.newScrollView
	{   
	    top = headBg.y+headBg.height/2+5,
	    left = -menuBg.width,
	    width = menuBg.width-1,
	    height = menuBg.height,
	    horizontalScrollDisabled = true,
	    verticalScrollDisabled = false,
	    isBounceEnabled =true,
	    hideBackground = true,
	    bottomPadding =100,
	}

		




	local function changePage(event)
		local phase = event.phase 

			event.target.alpha=0.5

			timer.performWithDelay( 50, function (  )
				event.target.alpha=0

				if event.target.page~=nil and event.target.page~="" and composer.getSceneName( "current")~=event.target.page then

					-- if (event.target.index>=3 and event.target.index<=7) and (myapp.userToken==nil or myapp.userToken=="") then

					-- 	composer.gotoScene("preview",{effect="fade",time=200})

					-- else
						composer.removeScene(composer.getSceneName( "current"))
						myapp.selectPage =event.target.index
						composer.gotoScene(event.target.page)
					-- end
				else
					composer.hideOverlay(true,"fade", 0 )
				end

			end)
		return true
	end


	for i=1,#menuListItem do

		local subCount =0
		menuGroups[i] = display.newGroup()

		menuBgTable[i+subCount] = display.newRect( 0, 0,menuBg.width-0.5 , 45 )  
		menuBgTable[i+subCount].anchorX=0
		menuBgTable[i+subCount]:setFillColor(0)  
		menuBgTable[i+subCount].x=display.screenOriginX
		menuBgTable[i+subCount].y=25+(i+subCount-1)*menuBgTable[i+subCount].height
		menuBgTable[i+subCount].alpha = 0
		menuBgTable[i+subCount].isHitTestable=true
		menuBgTable[i+subCount].index =i+subCount
		menuBgTable[i+subCount].page =menuListItem[i].page
		menuBgTable[i+subCount]:addEventListener( "tap",changePage )
		menuGroups[i]:insert(menuBgTable[i])


		menuIconTable[i+subCount] = display.newImageRect(myapp.image..myapp.menuIcon[i][1],20,20 )
		menuIconTable[i+subCount].anchorX=0
		menuIconTable[i+subCount].x =display.screenOriginX+30
		menuIconTable[i+subCount].y = menuBgTable[i+subCount].y
		menuGroups[i]:insert(menuIconTable[i+subCount])


		menuTable[i+subCount] = display.newText(menuListItem[i].name, 0, 0, native.systemFont, 13 )
		menuTable[i+subCount]:setFillColor( 0,0.5 )
		menuTable[i+subCount].anchorX=0
		menuTable[i+subCount].x =70
		menuTable[i+subCount].y = menuIconTable[i+subCount].y
		menuGroups[i]:insert(menuTable[i+subCount])

		if i==1 then
			menuNTable[i+subCount] = display.newText(tostring(myapp.mealCount), 0, 0, native.systemFont, 13 )
			menuNTable[i+subCount]:setFillColor( 0,0.5 )
			menuNTable[i+subCount].anchorX=1
			menuNTable[i+subCount].x =display.screenOriginX+menuBg.width-20
			menuNTable[i+subCount].y = menuIconTable[i+subCount].y
			menuGroups[i]:insert(menuNTable[i+subCount])

		end

		if i==2  then
			menuNTable[i+subCount] = display.newText(tostring(myapp.favoCount), 0, 0, native.systemFont, 13 )
			menuNTable[i+subCount]:setFillColor( 0,0.5 )
			menuNTable[i+subCount].anchorX=1
			menuNTable[i+subCount].x =display.screenOriginX+menuBg.width-20
			menuNTable[i+subCount].y = menuIconTable[i+subCount].y
			menuGroups[i]:insert(menuNTable[i+subCount])
		end


		if i==3 then
			menuNTable[i+subCount] = display.newText(tostring(myapp.videoCount), 0, 0, native.systemFont, 13 )
			menuNTable[i+subCount]:setFillColor( 0,0.5 )
			menuNTable[i+subCount].anchorX=1
			menuNTable[i+subCount].x =display.screenOriginX+menuBg.width-20
			menuNTable[i+subCount].y = menuIconTable[i+subCount].y
			menuGroups[i]:insert(menuNTable[i+subCount])

			if myapp.videoCount~=0 then
				menuNTable[i+subCount].text =tostring(myapp.videoCount)
			end
		end


		-- if i==4  then
		-- 	menuNTable[i+subCount] = display.newText(tostring(myapp.count.favoCount), 0, 0, native.systemFont, 13 )
		-- 	menuNTable[i+subCount]:setFillColor( 0,0.5 )
		-- 	menuNTable[i+subCount].anchorX=1
		-- 	menuNTable[i+subCount].x =display.screenOriginX+menuBg.width-20
		-- 	menuNTable[i+subCount].y = menuIconTable[i+subCount].y
		-- 	menuGroups[i]:insert(menuNTable[i+subCount])
		-- end



		if i==myapp.selectPage then
			local paint = {
			    type = "image",
			    filename =myapp.image..myapp.menuIcon[i][2]
			}
			menuIconTable[i+subCount].fill =paint
			-- menuBgTable[i+subCount].alpha = 0.1


			if menuBgTable[i+subCount]~=nil then

				menuBgTable[i+subCount]:removeSelf( )
				menuBgTable[i+subCount] =nil

				menuBgTable[i+subCount] = display.newImageRect("images/menu_select.png",40,40)				
				menuBgTable[i+subCount].anchorX=0
				menuBgTable[i+subCount].x=menuTable[i+subCount].x+menuTable[i+subCount].width-menuBgTable[i+subCount].width/2
				menuBgTable[i+subCount].alpha=0.9
				menuBgTable[i+subCount].y=menuTable[i+subCount].y
				menuBgTable[i+subCount].index =i+subCount
				menuBgTable[i+subCount].page =menuListItem[i].page
				menuBgTable[i+subCount]:addEventListener( "tap",changePage )
				menuGroups[i]:insert(menuBgTable[i])


			end
			-- if menuTable[i+subCount]~=nil then

			-- 	menuTable[i+subCount]:removeSelf( )
			-- 	menuTable[i+subCount] =nil
			-- 	menuTable[i+subCount] = display.newText(menuListItem[i].name, 0, 0, native.systemFont, 13 )
			-- 	menuTable[i+subCount]:setFillColor(myapp.mainColor[1],myapp.mainColor[2],myapp.mainColor[3])
			-- 	menuTable[i+subCount].anchorX=0
			-- 	menuTable[i+subCount].x =70
			-- 	menuTable[i+subCount].y = menuIconTable[i+subCount].y
			-- 	menuGroups[i]:insert(menuTable[i+subCount])

			-- end

			-- if menuNTable[i+subCount]~=nil then

			-- 	menuNTable[i+subCount]:removeSelf( )
			-- 	menuNTable[i+subCount] =nil

			-- 	menuNTable[i+subCount] = display.newText("", 0, 0, native.systemFont, 13 )
			-- 	menuNTable[i+subCount]:setFillColor(myapp.mainColor[1],myapp.mainColor[2],myapp.mainColor[3])
			-- 	menuNTable[i+subCount].anchorX=1
			-- 	menuNTable[i+subCount].x =display.screenOriginX+menuBg.width-20
			-- 	menuNTable[i+subCount].y = menuIconTable[i+subCount].y
			-- 	menuGroups[i]:insert(menuNTable[i+subCount])

			-- 	if i==1 then
			-- 		menuNTable[i+subCount].text =tostring(myapp.mealCount)
			-- 	end

			-- 	if i==2 then
			-- 		menuNTable[i+subCount].text =tostring(myapp.favoCount)
			-- 	end

			-- 	if i==3 then
			-- 		menuNTable[i+subCount].text =tostring(myapp.videoCount)
			-- 	end
			-- end

		end

		list:insert(menuGroups[i])

	end

	group:insert(list)

	local function onTouch( event )


		timer.performWithDelay( 50, function ( event )
			transition.to( menuBg, { time=150, x=-display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
			end} )
			
			transition.to( headBg, { time=150, x=-display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
			end} )

			transition.to( shadow, { time=150, x=-display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
			end} )


			transition.to( head, { time=150, x=-display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
			end} )

			transition.to( name, { time=150, x=-display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
			end} )

			transition.to( account, { time=150, x=-display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
			end} )


			transition.to( list, { time=150, x=-display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
			end} )
			transition.to( background, { time=150, alpha=0,transition=easing.linear, onComplete=function ( event )
				composer.hideOverlay(true,"fade", 0 )
			end} )
		



		end)

		return true

	end

	background:addEventListener( "tap", onTouch)




	
end


function scene:show( event )
   local group = self.view
   local phase = event.phase
   local parent = event.parent  --reference to the parent scene object

   if ( phase == "will" and parent~=nil ) then
      parent:overlayBegan()

      timer.performWithDelay( 0, function ( event )
      	transition.to( menuBg, { time=250, x=display.screenOriginX+display.actualContentWidth/1.25/2-1,transition=easing.outQuad, onComplete=function ( event )
      	end} )

      	transition.to( headBg, { time=250, x=display.screenOriginX+display.actualContentWidth/1.25/2-1,transition=easing.outQuad, onComplete=function ( event )
      	end} )

      	transition.to( shadow, { time=250, x=display.screenOriginX+display.actualContentWidth/1.25/2-1,transition=easing.outQuad, onComplete=function ( event )
      	end} )


      	transition.to( head, { time=250, x=display.screenOriginX+display.actualContentWidth/6-10,transition=easing.outQuad, onComplete=function ( event )
      	end} )

      	transition.to( name, { time=250, x=display.screenOriginX+15,transition=easing.outQuad, onComplete=function ( event )
      	end} )

      	transition.to( account, { time=250, x=display.screenOriginX+15,transition=easing.outQuad, onComplete=function ( event )
      	end} )


      	transition.to( list, { time=250, x=display.actualContentWidth/1.25/2,transition=easing.outQuad, onComplete=function ( event )
      	end} )


      	transition.to( background, { time=250, alpha=1,transition=easing.outQuad, onComplete=function ( event )

      		myapp.menuOpen =true
      	end} )



      end)
   end

end

function scene:hide( event )
   local group = self.view
   local phase = event.phase
   local parent = event.parent  --reference to the parent scene object
   if ( phase == "will" and parent~=nil  ) then
      	parent:overlayEnded()
      	background.alpha = 0

   end

end

function scene:destroy( event )
   local group = self.view
   myapp.menuOpen =false


end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene