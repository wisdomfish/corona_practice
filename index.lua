local composer = require( "composer" )
local scene = composer.newScene()
local menuList = require "scripts.menulist"


function scene:create( event )

    local group = self.view

    local titleBar = display.newRect( halfW, 0, display.actualContentWidth, display.actualContentHeight*0.075 )
    titleBar:setFillColor(0,0.2)
    titleBar.y = display.screenOriginY + titleBar.contentHeight * 0.5+display.topStatusBarContentHeight
    titleBar.strokeWidth=1
    group:insert(titleBar)

    local title = display.newText("店家", 0, 0,native.systemFont, 18 )
    title:setFillColor( 255 )
    title.anchorX=0
    title.x =60
    title.y = titleBar.y
    group:insert(title)




    --側邊欄
    local listButton = menuList.new(tonumber(titleBar.contentHeight),titleBar.y,"")
    group:insert(listButton)

end


function scene:show( event )

    local group = self.view
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
    end
end


function scene:hide( event )

    local group = self.view
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
    end
end


function scene:destroy( event )

    local group = self.view

end


function scene:overlayBegan( event )
end

function scene:overlayEnded( event )

  display.getCurrentStage():setFocus(nil)


end



scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


return scene