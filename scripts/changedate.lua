local changeDate = {}

--輸出毫秒，計算時秒
function changeDate.changAll(str)
	 local pattern = "(%d+)-(%d+)-(%d+) (%d+):(%d+):(%d+)"
     local timeToConvert =str
     local runyear, runmonth, runday, runhour, runminute, runseconds = timeToConvert:match(pattern)
	 local convertedTimestamp = os.time({year = runyear, month = runmonth, day = runday, hour = runhour, min = runminute, sec = runseconds})
     
     return convertedTimestamp
end

--更改日期格式 輸出 ex.2013/01/01
function changeDate.changJustDate(str)
     local pattern = "(%d+)/(%d+)/(%d+)"

     local timeToConvert =str
     local runyear, runmonth,runday = timeToConvert:match(pattern)
    
     return runyear.."/"..runmonth.."/"..runday
end


function changeDate.comp(str)

    local sp = split.split(str,"T")
    if sp~=nil and #sp~=0 and #sp>=2 then

        local i,j = string.gsub(sp[2],".000Z","")
        if i~=nil then
          sp[1]=sp[1].." "..i
        end
    else
        sp[1] =str
    end


    local A=changeDate.changAll(sp[1])
    local B=changeDate.changAll(os.date("%Y-%m-%d %H:%M:%S"))
    str=""
       if 1>((B-A)/(60*60)) then
            str=math.floor((B-A)/(60))
            if tonumber(str)>1 then
                str =str.." minutes ago"
            else
                str =str.." minute ago"
            end

       elseif 24>((B-A)/(60*60)) then 
            str=math.floor((B-A)/(60*60))
            if tonumber(str)>1 then
                str =str.." hours ago"
            else
                str =str.." hour ago"
            end

        elseif 7>((B-A)/(60*60*24)) then
            str=math.floor((B-A)/(60*60*24))
            if tonumber(str)>1 then
                str =str.." days ago"
            else
                str =str.." day ago"
            end
        elseif 5>((B-A)/(60*60*24*7)) then
            str=math.floor((B-A)/(60*60*24*7))
            if tonumber(str)>1 then
                str =str.." weeks ago"
            else
                str =str.." week ago"
            end
        elseif 12>((B-A)/(60*60*24*30)) then
            str=math.floor((B-A)/(60*60*24*30))
            if tonumber(str)>1 then
                str =str.." months ago"
            else
                str =str.." month ago"
            end
        else
            str=math.floor((B-A)/(60*60*24*30*12))
            if tonumber(str)>1 then
                str =str.." years ago"
            else
                str =str.." year ago"
            end


        end

    return str

end


function changeDate.replace(str)
    local sp = split.split(str,"T")
    if sp~=nil and #sp~=0 and #sp>=2 then

        local i,j = string.gsub(sp[2],".000Z","")
        if i~=nil then
          sp[1]=sp[1].." "..i
        end
    else
        sp[1] =str
    end

    return changeDate.changAll(sp[1])
end


--輸出毫秒，不計算時秒
function changeDate.changPart(str)

        words = {}
        for date in string.gmatch(str, "[^%s]+") do
            table.insert( words, date )
        end
        local pattern = "(%d+)/(%d+)/(%d+)"
        local timeToConvert =words[1]
        local runyear, runmonth, runday = timeToConvert:match(pattern)
        local convertedTimestamp = os.time({year = runyear, month = runmonth, day = runday})
        return convertedTimestamp
end

--轉換youtube 時間
function changeDate.covertYTBTime(str)
    
    local formatTime ={}
    local finalTime =""
    local i,j = string.gsub(str,"PT","")

    i,z = string.gsub(i,"H",":")
    i,x = string.gsub(i,"M",":")
    i,k = string.gsub(i,"S","")

    formatTime =split.split(i,":")

    --只有秒 0:50
    if z==0 and x==0 and k==1 then
        if #formatTime[1]<2 then
            formatTime[1]="0"..formatTime[1]   
        end
        finalTime="0:"..formatTime[1]

    --只有分 5:00
    elseif z==0 and x==1 and k==0 then
      if #formatTime[1]<2 then
          formatTime[1]="0"..formatTime[1]  
      end
      finalTime=formatTime[1]..":00"
      
    --只有時 1:00:00
    elseif z==1 and x==0 and k==0 then
      finalTime=formatTime[1]..":00"..":00"
    
    --只有時分 1:20:00
    elseif z==1 and x==1 and k==0 then
      if #formatTime[2]<2 then
          formatTime[2]="0"..formatTime[2]  
      end
      finalTime=formatTime[1]..":"..formatTime[2]..":00"

    --只有時秒 1:00:12
    elseif z==1 and x==0 and k==1 then
      if #formatTime[2]<2 then
          formatTime[2]="0"..formatTime[2]   
      end
      finalTime=formatTime[1]..":00"..":"..formatTime[2]

    --只有分秒 50:42
    elseif z==0 and x==1 and k==1 then
      if #formatTime[2]<2 then
          formatTime[2]="0"..formatTime[2]   
      end
      finalTime=formatTime[1]..":"..formatTime[2]

    --時分秒 1:24:12
    elseif z==1 and x==1 and k==1 then
      if #formatTime[2]<2 then
          formatTime[2]="0"..formatTime[2] 
      end

      if #formatTime[3]<2 then
          formatTime[3]="0"..formatTime[3]   
      end
      finalTime=formatTime[1]..":"..formatTime[2]..":"..formatTime[3]

    end

    return finalTime
end


return changeDate