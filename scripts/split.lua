local M={}


--分割string
function M.split(value,delimiter)
    local result = {}
    local from  = 1
    local delim_from, delim_to = string.find( value, delimiter, from  )
    while delim_from do
        table.insert( result, string.sub( value, from , delim_from-1 ) )
        from  = delim_to + 1
        delim_from, delim_to = string.find( value, delimiter, from  )
    end
    table.insert( result, string.sub( value, from  ) )
    return result
end

return M;
