local openssl = require "plugin.openssl"
local mime = require ( "mime" )

local aes={}

function aes.encrypt(value,key)

 	local cipher = openssl.get_cipher ( "aes-128-cbc" )
 	local encryptedData = mime.b64(cipher:encrypt ( value, key,key ))

	return encryptedData

end

function aes.decrypt(value,key)

	local cipher = openssl.get_cipher ( "aes-128-cbc" )
	local decryptedData = cipher:decrypt ( mime.unb64 ( value ), key,key )


	return decryptedData

end

return aes
