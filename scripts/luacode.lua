	local luacode = {}

	-- 處理中文字
	function luacode.truncateUTF8String(s, n)
	  local r = string.sub(s, 1, n)
	  local last = string.byte(r, n)
	  if not last then return r end
	  while last >= 128 and last <= 192 do
	    n = n - 1
	    r = string.sub(r, 1, n)
	    last = string.byte(r, n)
	  end
	  if last >= 128 then
	    r = string.sub(r, 1, n-1)
	  end
	  return r
	end

	-- url編碼
	function luacode.urlencode(str)
	   if (str) then
	      str = string.gsub (str, "\n", "\r\n")
	      str = string.gsub (str, "([^%w ])",
	         function (c) return string.format ("%%%02X", string.byte(c)) end)
	      str = string.gsub (str, " ", "%%20")
	   end
	   return str    
	end

	-- 檢查nil
	function luacode.checknil(str,str1)
		if (str ==nil) then
			str=str1	
		else
			str=str
		end
		return str
	end
    
	function luacode.compStringAsc( tags )
		-- for i=1,#tags do
        count=0 --計算tag長度
        l=0 --判斷是否為中文
            for j=1,#tags do 
                -- if(tags[i]:sub(j,j):byte()>127)then
                if(tags:sub(j,j):byte()>127)then
                    l=l+1
                    if(l==3)then
                        count=count+13
                        -- print("中文")
                        l=0
                    end
                else
                	if(tags:sub(j,j):byte() >65 and tags:sub(j,j):byte()<90)then

                        count=count+10
                       -- print("大寫字母"..tags:sub(j,j):byte())

                    else
                    	count=count+8
                    	-- print("數字或小寫字母")
                     end
                end
                 
            end
         -- end
         return count
	end


	function luacode.countStringLen( tags )
        count=0 
        l=0 
        
        for j=1,#tags do 
                -- if(tags[i]:sub(j,j):byte()>127)then
                if(tags:sub(j,j):byte()>127)then
                    l=l+1
                    if(l==3)then
                        count=count+1
                            -- print("中文")
                        l=0
                    end
                else
                	if(tags:sub(j,j):byte() >65 and tags:sub(j,j):byte()<90)then

 						count=count+1
                       -- print("大寫字母"..tags:sub(j,j):byte())

                    else
                    	count=count+1
                    
                     end
                end
                 
            end

         return count
	end

    --截取部分字串
    --len:截取最大len個字元
    function luacode.subString( tags,len,nop)
            l=0 
            local ss=""
            local tab = {0,0}
            local lens =0
            for j=1,#tags do 
                    -- if(tags[i]:sub(j,j):byte()>127)then
                    if(tags:sub(j,j):byte()>127)then
                        l=l+1
                        if(l==3)then         
                            l=0
                            lens =lens+1
                            tab[1]=tab[1]+1
                        end
                    else
                        --大寫字母
                        if(tags:sub(j,j):byte() >=65 and tags:sub(j,j):byte()<=90)then
                           lens =lens+1
                           tab[2]=tab[2]+1

                        --小寫
                        elseif(tags:sub(j,j):byte() >=95 and tags:sub(j,j):byte()<=122)then
                            lens =lens+0.5
                            tab[2]=tab[2]+1
                        --數字
                        else
                            lens =lens+0.7
                            tab[2]=tab[2]+1

                        end
                    end
                if math.modf(lens)>len  then
                    if nop==nil then
                        ss="..."
                    end
                    break
                end
            end
             return string.sub(tags,0,tab[1]*3+tab[2])..ss
        end

    function luacode.newChar( tags )
        count=0 
        for j=1,#tags do 
                   
            count = tags:sub(j,j):byte()    
        end

         return count
    end


    --數字加上逗點
    function luacode.coin(amount)
      local formatted = amount
      while true do  
        formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
        if (k==0) then
          break
        end
      end
      return formatted
    end




    function getWordTable(str)
        local pos = 1
        local words = {}
     
        while ( pos ) do
     
            local s,r = string.find(str,'%w+',pos)
     
            if ( s ) then
                pos = r + 1
                table.insert(words,string.sub(str,s,r))
            else
                break
            end
     
        end
     
        return words
     
    end

    --首字轉換成大寫
    function luacode.firstUpper(str)
        local words = getWordTable(str)
        local result = ''
        for i, v in ipairs(words) do
            words[i] = string.lower(words[i])
            local tmp = string.upper( string.sub(words[i],1,1)) ..
             string.sub(words[i],2)
            result = result .. tmp .. ' '
        end
     
        return (string.gsub(result, "^%s*(.-)%s*$", "%1")) 
    end







	return luacode