local composer = require( "composer" )
local scene = composer.newScene()
local myapp = require "scripts.myapp"
local widget = require "widget"
local timeC =nil
local tran =nil
local tran2 =nil
function scene:create( event )

    local group = self.view
    local isVideoDetail =false --檢查是不是影片內頁所觸發
    local showTime =1000 --顯示的時間
    if event.params.time ~=nil then
        showTime =event.params.time
    end
    local alertBg = display.newRoundedRect(0, 0, 50, 30,2 )
    alertBg:setFillColor(0,0.9)
    alertBg.x = halfW
    alertBg.y = display.contentHeight-20
    alertBg.anchorY=1
    group:insert(alertBg)

    local tip = display.newText(event.params.text, 0, 0,native.systemFont, 12 )
    tip:setFillColor(1)
    tip.x =alertBg.x
    tip.y = alertBg.y-0.5-alertBg.height/2
    group:insert(tip)


    alertBg.width = tip.width+20
    timeC = timer.performWithDelay( showTime, function ()
       tran = transition.to( alertBg, { time=500, alpha=0, onComplete=function (  )
            if alertBg~=nil then
                display.remove( alertBg )
                alertBg=nil
            end

        end } )
       tran2 = transition.to( tip, { time=500, alpha=0, onComplete=function (  )
            composer.hideOverlay(true,"fade", 0 )
            if tip~=nil then
                display.remove( tip )
                tip =nil
            end


        end } )

    end)

end


function scene:show( event )

    local group = self.view
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
    end
end


function scene:hide( event )

    local group = self.view
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
    end
end


function scene:destroy( event )

    local group = self.view
    timer.cancel( timeC )
    transition.cancel( tran2 )
end


scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


return scene