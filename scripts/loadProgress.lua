local composer = require( "composer" )
local scene = composer.newScene()
local myapp = require "scripts.myapp"
local widget = require "widget"

function scene:create( event )

    local group = self.view

    local options = {
        width = 40,
        height = 40,
        numFrames = 1,
        sheetContentWidth = 40,
        sheetContentHeight = 40
    }
    local spinnerMultiSheet = graphics.newImageSheet( "images/sp.png", options )

    local sp = widget.newSpinner
    {   
        left = halfW-40/2,
        top = halfH,
        width = 40,
        height = 40,
        sheet = spinnerMultiSheet,
        startFrame = 1,
        deltaAngle = -10,
        incrementEvery = 20

    }
    sp:start()

    -- sp = widget.newSpinner
    -- {
    --     left = halfW-25/2,
    --     top = halfH,
    --     width=25, 
    --     height=25,
    -- } 
    -- sp:start( )
    -- sp.isVisible=true
    group:insert(sp)
end


function scene:show( event )

    local group = self.view
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
    end
end


function scene:hide( event )

    local group = self.view
    local phase = event.phase

    if ( phase == "will" ) then
    elseif ( phase == "did" ) then
        -- timer.cancel( time )
    end
end


function scene:destroy( event )

    local group = self.view
end


scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


return scene