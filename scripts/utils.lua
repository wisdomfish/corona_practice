
-----------------------------------------------------------------------------------------
--
-- utils.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local widget = require( "widget" )
local json = require( "json" )

local F = {};

-- 刪除字串前後空白字元 
function F.trim( s )
   return string.match( s,"^()%s*$") and "" or string.match(s,"^%s*(.*%S)" )
end

-- read Json file
function F.jsonFile( filename, base )
	-- set default base dir if none specified
	if not base then base = system.ResourceDirectory; end

	-- create a file path for corona i/o
	local path = system.pathForFile( filename, base )
	if ( not path ) then return nil; end

	-- will hold contents of file
	local contents

	-- io.open opens a file at path. returns nil if no file found
	local file = io.open( path, "r" )
	if ( file ) then
		-- read all contents of file into a string
		contents = file:read( "*a" )
		io.close( file ) -- close the file after using it
	else
		return nil
	end

	return contents
end

-- 判斷是否為䦌年
function F.isLunar( year )
	return ( math.mod(year, 4) == 0 and math.mod(year, 100) ~= 0 ) or
		   ( math.mod(year, 100) == 0 and math.mod(year, 400) == 0 ) 
end

-- 依 year, month 取得該月天數
function F.getDaysOfTheMonth( y, m )	
	local daysOfFeb = 28
	if ( F.isLunar( y ) ) then daysOfFeb = 29 end

	local days = 31
	if ( m == 2 ) then days = daysOfFeb end
	if ( m == 4 or m == 6 or m == 9 or m == 11 ) then days = 30 end

	return days
end

-- 解析 json file
function F.getJsonData( file )
	local jsonStr = F.jsonFile( file )
	if ( not jsonStr ) then 
		return false, nil
	end

	local data = json.decode( jsonStr )

	-- 查無 json 相關資訊
	if ( not data or 0 == #data.list ) then 
		return false, nil
	end

	return true, data
end

-- 取得未讀公告數
function F.getBulletinUnReadCount()
	-- 取得公告資料
	local ok, bulletin = F.getJsonData( "assets/data/bulletin.json" )
	if ( not ok ) then 
		return nil
	end

	local count = 0
	for k, v in pairs( bulletin.list ) do
		if ( not v.isRead ) then count = count + 1 end
	end

	return count
end

-- 依值取得該值在 table 中的 index
-- return nil 表示 value 不在 table 中
function F.getIndexByVal( table, value )
	print("table, value:", table, value)
	if ( not value or "" == value ) then return nil end

	for k, v in pairs( table ) do
		print("k,v:",k, v)
		if ( v == value) then return k end
	end

	return nil
end

return F;
