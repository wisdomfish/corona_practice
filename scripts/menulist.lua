local menulist = {}
local widget = require( "widget" )
local composer = require( "composer" )

function menulist.new(str,titleY)
    local listButtonClickArea
    local listButton

    if display.topStatusBarContentHeight==0 then
        str =  str +display.topStatusBarContentHeight
    else
        str =  str +display.topStatusBarContentHeight
    end
    local options =
    {
        effect = "fade",
        time =0, 
        isModal = true,
          params =
		  {
                tHeight =str
		  }
    }

	local widgetGroup = display.newGroup()

	local function onListButton( event )

        if event.phase =="began" then
            
            if listButtonClickArea then
                listButtonClickArea:setFillColor( 1,0.1 )
            end
        elseif event.phase =="ended" then

            if listButtonClickArea then
                listButtonClickArea:setFillColor(1,0 )
            end
            if myapp.menuOpen==false  then
                composer.showOverlay( "menu", options )
                timer.performWithDelay( 300,function ()
                    myapp.menuOpen=true 
                end)


            end

            -- timer.performWithDelay( 100, function ( event )
                -- transition.to( obj, { time=250, x=display.actualContentHeight/5-2,transition=easing.outSine} )
                
            -- end)

        end

	end
		
	listButton = display.newImageRect("images/public_left_button.png",32,30 )
    listButton.anchorX = 0
    listButton.anchorY = 0.5
    listButton.x = display.screenOriginX+10
    listButton.alpha=1
    listButton.y = titleY

    --導航按鈕
	listButtonClickArea = display.newRect( 0, listButton.y, 55, str ) 
    listButtonClickArea:setFillColor(0,0)
    listButtonClickArea.isHitTestable =true
    listButtonClickArea.anchorX=0
    listButtonClickArea:addEventListener( "touch", onListButton )
	widgetGroup:insert(listButton)
	widgetGroup:insert(listButtonClickArea)

	return widgetGroup
end

return menulist