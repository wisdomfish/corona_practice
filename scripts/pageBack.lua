local M = {}
local composer = require( "composer" )

--頁面儲存佇列
function M.addToBack()
    if #myapp.reSce==0 then
            table.insert(myapp.reSce,composer.getSceneName( "current" ))
        else
            local find=false
            for k, v in pairs(myapp.reSce) do
                if v==composer.getSceneName( "current" ) then
                  find=true

                  table.remove(myapp.reSce,k)
                  table.insert(myapp.reSce,composer.getSceneName( "current" ))
                  break
                end
            end
            if find==false then
              table.insert(myapp.reSce,composer.getSceneName( "current" ))
        end
    end

end

--回上一頁
function M.backPrePage()

  
  if myapp.reSce[#myapp.reSce]~=nil then
    composer.removeScene(myapp.reSce[#myapp.reSce])
    table.remove(myapp.reSce)
    composer.gotoScene(myapp.reSce[#myapp.reSce],"fade",200)
  end

end


return M